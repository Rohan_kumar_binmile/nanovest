import React from 'react';
import {AppRegistry} from 'react-native';
import {Provider} from 'react-redux';
import {Provider as PaperProvider} from 'react-native-paper';
import {PersistGate} from 'redux-persist/integration/react';
import App from './App';
import configureStore from './src/store';
import {name as appName} from './app.json';

const storeObject = configureStore();
const {store, persistor} = storeObject;

export default function Main() {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <PaperProvider>
          <App />
        </PaperProvider>
      </PersistGate>
    </Provider>
  );
}

AppRegistry.registerComponent(appName, () => Main);
