import React, {useState} from 'react';
import {View, FlatList} from 'react-native';
import {useSelector} from 'react-redux';
import RadioButtonGroup from '../component/RadioButtonGroup';
import CointListitem from '../component/CointListitem';
import styles from './Styles';

const WatchlistScreen = props => {
  const {navigation} = props;
  // INFO: value for radion button of view
  const LIST_VIEW = 'LIST_VIEW';
  const GRID_VIEW = 'GRID_VIEW';

  // INFO: component states
  const [checked, setChecked] = useState(LIST_VIEW);

  const {watchListData} = useSelector(state => state.userSlice);

  const onHandleSelectItem = item => {
    navigation.navigate('DetailScreen', {detail: item});
  };

  const onHandleListView = () => {
    return checked === LIST_VIEW ? (
      <FlatList
        key={'_'}
        data={watchListData}
        renderItem={({item}) => (
          <CointListitem
            key={item.symbol}
            item={item}
            onHandleSelectItem={onHandleSelectItem}
          />
        )}
        keyExtractor={item => item.symbol}
        numColumns={1}
      />
    ) : (
      <FlatList
        key={'#'}
        data={watchListData}
        renderItem={({item}) => (
          <CointListitem
            key={item.symbol}
            item={item}
            onHandleSelectItem={onHandleSelectItem}
          />
        )}
        keyExtractor={item => item.symbol}
        numColumns={2}
      />
    );
  };

  return (
    <View style={[styles.container]}>
      <RadioButtonGroup
        checked={checked}
        listViewValue={LIST_VIEW}
        gridViewValue={GRID_VIEW}
        setChecked={newValue => setChecked(newValue)}
      />
      <View style={styles.dataContainer}>{onHandleListView()}</View>
    </View>
  );
};

export default WatchlistScreen;
