import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  dataContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  radioButtonContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  radioButtonView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  errorText: {
    marginVertical: 0,
    paddingVertical: 0,
  },
  buttonText: {
    alignSelf: 'center',
    fontSize: 17,
  },
  buttonView: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10,
  },
  image: {
    height: 250,
    marginBottom: 40,
    width: 250,
  },
  signUpTextMargin: {
    marginTop: 0,
  },
});

export default styles;
