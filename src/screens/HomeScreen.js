import React, {useEffect, useState} from 'react';
import {View, FlatList, ActivityIndicator} from 'react-native';
import axios from 'axios';
import RadioButtonGroup from '../component/RadioButtonGroup';
import CointListitem from '../component/CointListitem';
import dummyData from '../utils/Constant';
import styles from './Styles';

const HomeScreen = props => {
  const {navigation} = props;
  // INFO: value for radion button of view
  const LIST_VIEW = 'LIST_VIEW';
  const GRID_VIEW = 'GRID_VIEW';

  // INFO: component states
  const [listData, setListData] = useState([]);
  const [checked, setChecked] = useState(LIST_VIEW);
  const [loaderState, setLoaderState] = useState(false);

  // INFO: coin list API call
  const onHandleAPICall = () => {
    const dataList = [];
    setLoaderState(true);
    const APIURL =
      'http://api.coinlayer.com/api/list?access_key=1935fb1087e519cd0092976a6c34426d';
    axios
      .get(APIURL)
      .then(response => {
        setLoaderState(false);
        setListData(response.data);
        if (response.data && response.data?.crypto) {
          Object.keys(response.data.crypto).map(function (key) {
            dataList.push(response.data.crypto[key]);
          });
          setListData(dataList);
        }
      })
      .catch(error => {
        setLoaderState(false);
        console.log('Error :', error);
      });
  };

  useEffect(() => {
    onHandleAPICall();
  }, []);

  const onHandleSelectItem = item => {
    navigation.navigate('DetailScreen', {detail: item});
  };

  const onHandleShowWatchlist = () => {
    navigation.navigate('WatchlistScreen');
  };

  const onHandleListView = () => {
    return checked === LIST_VIEW ? (
      <FlatList
        key={'_'}
        data={listData}
        renderItem={({item}) => (
          <CointListitem
            key={item.symbol}
            item={item}
            onHandleSelectItem={onHandleSelectItem}
          />
        )}
        keyExtractor={item => item.symbol}
        numColumns={1}
      />
    ) : (
      <FlatList
        key={'#'}
        data={listData}
        renderItem={({item}) => (
          <CointListitem
            key={item.symbol}
            item={item}
            onHandleSelectItem={onHandleSelectItem}
          />
        )}
        keyExtractor={item => item.symbol}
        numColumns={2}
      />
    );
  };

  return (
    <View style={[styles.container]}>
      <RadioButtonGroup
        checked={checked}
        isButtonEnable
        listViewValue={LIST_VIEW}
        gridViewValue={GRID_VIEW}
        setChecked={newValue => setChecked(newValue)}
        onHandleShowWatchlist={onHandleShowWatchlist}
      />
      <View style={styles.dataContainer}>
        {loaderState ? (
          <ActivityIndicator size="large" color="#0000ff" />
        ) : (
          onHandleListView()
        )}
      </View>
    </View>
  );
};

export default HomeScreen;
