import React, {useEffect, useState} from 'react';
import {View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {
  addCoinInWatchList,
  removeCoinFromWatchList,
} from '../store/reducers/userSlice';
import CoinDetailCard from '../component/CoinDetailCard';
import styles from './Styles';

const DetailScreen = props => {
  const {route} = props;
  const [coinDetail, setCoinDetail] = useState({});
  const dispatch = useDispatch();
  const {watchListData} = useSelector(state => state.userSlice);

  useEffect(() => {
    if (route.params && route.params?.detail) {
      setCoinDetail(route.params.detail);
    }
  }, []);

  const onHandleAddItemInWatchList = () => {
    if (watchListData.some(item => item.symbol === coinDetail.symbol)) {
      const filteredWatchlist = watchListData.filter(
        item => item.symbol !== coinDetail.symbol,
      );
      dispatch(removeCoinFromWatchList(filteredWatchlist));
    } else {
      dispatch(addCoinInWatchList(coinDetail));
    }
  };

  return (
    <View style={[styles.container]}>
      <CoinDetailCard
        coinDetail={coinDetail}
        checkInList={watchListData.some(
          item => item.symbol === coinDetail.symbol,
        )}
        onHandleAddItemInWatchList={onHandleAddItemInWatchList}
      />
    </View>
  );
};

export default DetailScreen;
