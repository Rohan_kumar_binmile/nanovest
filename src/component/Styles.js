import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  radioButtonContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  radioButtonView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  cardView: {
    marginHorizontal: 10,
    marginVertical: 5,
    flex: 1,
  },
  cardTitle: {
    height: 50,
    width: 50,
    borderRadius: 5,
  },
  cardDetailView: {
    marginHorizontal: 10,
    marginVertical: 5,
  },
  buttonStyle: {
    marginHorizontal: 5,
  },
});

export default styles;
