import React from 'react';
import {Card} from 'react-native-paper';
import styles from './Styles';

const CointListitem = props => {
  const {item, onHandleSelectItem} = props;

  return (
    <Card style={styles.cardView} onPress={() => onHandleSelectItem(item)}>
      <Card.Title
        title={item.name}
        left={props => (
          <Card.Cover
            style={styles.cardTitle}
            {...props}
            source={{uri: item.icon_url}}
          />
        )}
      />
    </Card>
  );
};

export default CointListitem;
