import React from 'react';
import {Button, Card, Title, Paragraph} from 'react-native-paper';
import styles from './Styles';

const CoinDetailCard = props => {
  const {coinDetail, checkInList, onHandleAddItemInWatchList} = props;

  return (
    <Card style={styles.cardDetailView}>
      <Card.Cover source={{uri: coinDetail.icon_url}} />
      <Card.Content>
        <Title>{`coin symbol : ${coinDetail.symbol}`}</Title>
        <Paragraph>{`coin full name : ${coinDetail.name_full}`}</Paragraph>
        <Paragraph>{`coin max supply : ${coinDetail.max_supply}`}</Paragraph>
      </Card.Content>
      <Card.Actions>
        <Button onPress={onHandleAddItemInWatchList}>
          {checkInList ? 'Remove From Watchlist' : 'Add In Watchlist'}
        </Button>
      </Card.Actions>
    </Card>
  );
};

export default CoinDetailCard;
