import React from 'react';
import {View} from 'react-native';
import {useSelector} from 'react-redux';
import {RadioButton, Text, Button} from 'react-native-paper';
import styles from './Styles';

const RadioButtonGroup = props => {
  const {
    checked,
    setChecked,
    listViewValue,
    gridViewValue,
    onHandleShowWatchlist,
    isButtonEnable,
  } = props;

  const {watchListData} = useSelector(state => state.userSlice);

  const showWatchListButton = () => {
    return watchListData.length !== 0 ? (
      <View style={[styles.radioButtonView]}>
        <Button style={styles.buttonStyle} onPress={onHandleShowWatchlist}>
          Show Watchlist
        </Button>
      </View>
    ) : (
      <></>
    );
  };

  return (
    <RadioButton.Group
      onValueChange={newValue => setChecked(newValue)}
      value={checked}>
      <View style={styles.radioButtonContainer}>
        <View style={styles.radioButtonView}>
          <RadioButton value={listViewValue} />
          <Text>List View</Text>
        </View>
        <View style={styles.radioButtonView}>
          <RadioButton value={gridViewValue} />
          <Text>Grid View</Text>
        </View>
        {isButtonEnable ? showWatchListButton() : <></>}
      </View>
    </RadioButton.Group>
  );
};

export default RadioButtonGroup;
