/* eslint-disable no-param-reassign */
import {createSlice} from '@reduxjs/toolkit';

export const userSlice = createSlice({
  name: 'user',
  initialState: {
    user: null,
    temp: 1,
    userDetail: [{email: 'admin@admin.com', password: 'Admin@123'}],
    watchListData: [],
  },
  reducers: {
    addUser: (state, action) => {
      state.user = action.payload;
    },
    addUserList: (state, action) => {
      state.userDetail = [...state.userDetail, action.payload];
    },
    addCoinInWatchList: (state, action) => {
      if (
        !state.watchListData.some(item => item.symbol === action.payload.symbol)
      ) {
        state.watchListData = [...state.watchListData, action.payload];
      } else {
        state.watchListData = [...state.watchListData];
      }
    },
    removeCoinFromWatchList: (state, action) => {
      state.watchListData = action.payload;
    },
    clearUserStore: state => {
      state.user = null;
    },
  },
});

// Action creators are generated for each case reducer function
export const {
  addUser,
  addUserList,
  clearUserStore,
  addCoinInWatchList,
  removeCoinFromWatchList,
} = userSlice.actions;

export default userSlice.reducer;
