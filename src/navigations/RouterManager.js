import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import HomeScreen from '../screens/HomeScreen';
import DetailScreen from '../screens/DetailScreen';
import WatchlistScreen from '../screens/WatchlistScreen';

const Stack = createNativeStackNavigator();

const RouterManager = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="HomeScreen" component={HomeScreen} />
        <Stack.Screen name="DetailScreen" component={DetailScreen} />
        <Stack.Screen name="WatchlistScreen" component={WatchlistScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default RouterManager;
