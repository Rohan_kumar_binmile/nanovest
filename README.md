# pelocal_mobile

## Development Setup In Android

**Installing dependencies**

* You will need Node, the React Native command line interface, a JDK, and Android Studio.  
* You will need to install Android Studio in order to set up the necessary tooling to build your React Native app for Android.

* Install Node 12 or newer.
* Install and set up Java Development Kit

  ```
  Project requires at least the version 8 of the Java SE Development Kit (JDK)
  ```

**Install Android Studio**

* the following items are checked:

* Android SDK
* Android SDK Platform
* Android Virtual Device

  Select the "SDK Platforms" tab from within the SDK Manager, then check the box next to "Show Package Details" in the bottom right corner. Look for and expand the Android 10 (Q) entry, then make sure the following items are checked:

  * Android SDK Platform 29
  * Intel x86 Atom_64 System Image or Google APIs Intel x86 Atom System Image 
  
  Next, select the "SDK Tools" tab and check the box next to "Show Package Details" here as well. Look for and expand the "Android SDK Build-Tools" entry, then make sure that 29.0.2 is selected.

  Finally, click "Apply" to download and install the Android SDK and related build tools.

**Configure the ANDROID_HOME environment variable**

* Add the following lines to your $HOME/.bash_profile or $HOME/.bashrc (if you are using zsh then ~/.zprofile or ~/.zshrc) config file:
  
  ```
  export ANDROID_HOME=$HOME/Android/Sdk
  export PATH=$PATH:$ANDROID_HOME/emulator
  export PATH=$PATH:$ANDROID_HOME/tools
  export PATH=$PATH:$ANDROID_HOME/tools/bin
  export PATH=$PATH:$ANDROID_HOME/platform-tools
  ```

## Development Setup In IOS

* You will need Node, Watchman, the React Native command line interface, Xcode and CocoaPods.
* While you can use any editor of your choice to develop your app, you will need to install Xcode in order to set up the necessary tooling to build your React Native app for iOS.

**Node & Watchman**

* We recommend installing Node and Watchman using Homebrew. Run the following commands in a Terminal after installing Homebrew:

  ```
  brew install node
  brew install watchman
  ```
**Xcode**

* The easiest way to install Xcode is via the Mac App Store. Installing Xcode will also install the iOS Simulator and all the necessary tools to build your iOS app.
* If you have already installed Xcode on your system, make sure it is version 10 or newer.

**Command Line Tools**

* You will also need to install the Xcode Command Line Tools. Open Xcode, then choose "Preferences..." from the Xcode menu. Go to the Locations panel and install the tools by selecting the most recent version in the Command Line Tools dropdown.

**Installing an iOS Simulator in Xcode**

* To install a simulator, open Xcode > Preferences... and select the Components tab. Select a simulator with the corresponding version of iOS you wish to use.

**CocoaPods**

* CocoaPods is built with Ruby and it will be installable with the default Ruby available on macOS. You can use a Ruby Version manager, however we recommend that you use the standard Ruby available on macOS unless you know what you're doing.
* Using the default Ruby install will require you to use sudo when installing gems. (This is only an issue for the duration of the gem installation, though.)

  ```
  sudo gem install cocoapods
  ```


## How to run this porject.

*  Install `npm`
*  Use `npm install` from root directory to install dependencies. 
*  To start the metro server use : `npx react-native start`.
*  To run the project use : `npx react-native run-android` or `npx react-native run-ios`.

